'use strict';

module.exports = {
  name: 'ember-cli-dynamic-model',
  isDevelopingAddon: function() {
    return true;

  },
  options: {
    babel: {
      plugins: ['transform-object-rest-spread']
    }
  }
};
