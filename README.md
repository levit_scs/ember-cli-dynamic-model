# LevIT's Ember-cli-dynamic-model

Addon to dynamically load models source code.

:warning: This work is still in alpha stage.

We've not hit the 0.2.x series, what does it mean? The 0.2.x series will be dedicated to cleaning up code and adding tests. It's most likely the last series before reaching 1.0.0

## What's next?

- 1.0.x series when there will be a "satisfactory" amount of tests

## Installation

* `git clone` https://bitbucket.org/levit_scs/ember-cli-dynamic-model.git
* `npm install`
* `bower install`

## Usage

This addon provides a `modelLoader` service that has the ability to load (only once) and register
ember-data models dynamically. This addon is being designed as part of a larger project, an
auto-discovering admin interface and is meant at first to work with a Django backend, therefore,
models are expected to be part of "applications" (this constraint may be removed in the furtue).

First inject the service:

```
  modelLoader: Ember.inject.service()
```

Then, when a dynamic model is required, simply make sure it is loaded before using it

```
    this.get('modelLoader').ensure_model('app_name', 'model_name_singular', 'model_name_plural');
```

`ensure_model` returns a promise that will resolve once a model and all its dependencies are loaded.
`ensure_model` will also issue an `OPTIONS` call to the models list route. If the json response to this call has a `needs` property, `ensure_model` will recursively call itself for all models listed in that property.

Example `OPTIONS` response:

```
{
  "needs": [
    {
      "app": "catalog",
      "singular": "category",
      "plural": "categories"
    }, {
      "app": "auth",
      "singular": "user",
      "plural": "users"
    }
  ]
}
```

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://ember-cli.com/](http://ember-cli.com/).
